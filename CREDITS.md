Lead coding: Yannick Gingras

Additionnal coding:
- JT Wright
- Ryan Prior
- Nicole Schwartz
- Steve Smith
- Angel Hudgins
- Josephine Maya Simple
- Victor Hunt
- Chris J. Wallace
- Rodrigo Espinosa de los Monteros
- Boris Vorobev

Main testing:
- Colin Etienne Jean-Marie "cwpute" Landreau

Additionnal testing:
- Obigre
- Yellowsilver
- Zucadragon

DialogueManager plugin: MIT license by Nathan Hoad

2D tiles: CC0, released by the Craw Stone Soup team
https://opengameart.org/content/dungeon-crawl-32x32-tiles
https://github.com/crawl/tiles/tree/master/releases

Bestiary and NPC images:
- Le Grand Salapou, Éguis, Large Automata: CC-BY-SA by Jonathan Timmons, 2021
- Pridout Fauchève, Sulat Tiger, Sahwakoon: CC-BY by Clara Baltoré Pooter, 2021
- Henry Bessemer, Nochorts, Sentry Scarabs: CC-BY by Tuyên Đặng, 2021
- Labras, Pacherr, Phantruch (lesser and greater), Gleugt, Desert Centipede, Algerian Giant Locust, Cherub, Yarohu: Creative Commons Attribution 4.0 International License by Jason Teves, 2021
- Skeleton: public domain, by Hartmann Schedel (1440-1514)
- Sewer Alligator: public domain, by Pearson Scott Foresman

Splash Screen:
- CC-BY-SA, Zanya Fernández Rodríguez

Free Sans font:
- Copyright 2002-2012, GNU Freefont contributors, released under GNU General Public License v3 or later

Symbola_hint.ttf:
- Copyright (C) 2007-2015 George Douros
- "Fonts are free for any use; they may be opened, edited, modified, regenerated, packaged and redistributed."

Shield outline on the launch icon:
Public Domain by OpenClipart
https://freesvg.org/vector-drawing-of-blank-silver-shield

Icons in assets/opencliparts:
- CC0 by the Open Cliparts project. More info at http://www.openclipart.org

Combat Sound Effects:
CC0 by Ben (artisticdude), rubberduck, and faxcorp
https://opengameart.org/content/rpg-sound-pack
https://opengameart.org/content/100-cc0-sfx
https://opengameart.org/content/electricity-game-sound-pack

Additionnal sound effects CC-BY-SA Yannick Gingras:
- end screens sound effects
- hammer-01
- stick-01
- ethereal-01, 02
- razor
- explosion-01
- magic-01, 02

bottle-break: CC-BY 3.0 by spookymodem
https://opengameart.org/content/breaking-bottle

Particle sprites:
CC0 by Kenney
https://www.kenney.nl/

Room layouts are CC0 from Zorbus: http://dungeon.zorbus.net/

The historical map of Lyon in 1855 is Public Domain, digitize by Archives Municipales de Lyon.

Revengate is made with the Godot game engine.
- button icons are also from Godot
- https://godotengine.org/license/
