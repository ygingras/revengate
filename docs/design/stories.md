Revengate Stories
=================

Long narrations and short stories set in the Revengate universe. See also `src/story/*.txt`.


# The Audition
[moved here](../../src/story/the_audition.md)

# The Initiation

There were a dozen students sitting around the table, all bent over large leather bound books. They were all in their early twenties and judging by their attires, they came from all walks of life.

An older man entered the room. "Neophytes," he called, "Brother de Lévi is here for your aptitude test."

Brother de Lévi followed. We was in his early forties and was wearing a dark robe. On his shoulder was a leather satchel and in his hand was a long cane with an intricate large pommel. He placed both on the table before taking a seat. The older man who had introduced him retreated to a corner of the room.

De Lévi studied the room while the students exchanged excited glances and closed their books.

"I would like you to close your eyes," de Lévi called, "and thing back to the brightest fire you've ever seen. For some of you, it is going to be a bonfire at the solstice festival, some of you will think of the coal box on a great steam engine, and maybe a lucky few fill remember the roaring furnace of a steal foundry."

De Lévi retrieved a notebook, a fountain pen, and a glass rod from his satchel.

"Remember the color of the fire," he continued, "remember the smell, and its heat on your face. Bring the fire in front of you and hold it between your hands."

Without opening their eyes, the students slowly moved their arms. Some held their arms wide apart, some had their hand cupped in a tight ball, and there was everything in between.

"Breath deeper now," de Lévi called, "and fan those flames. Make them grow brighter... hotter... stronger..."

The faces of the students came alive with their effort. Some brows became wrinkly from being pushed together, some mouths frowned while some opened to eased a laboured breathing. De Lévi scribbled some notes.

"Brighter... hotter... stronger..." he reminded them.

The only sounds in the room were deep breathing and a faint ticking that came from de Lévi's cane. It was not as fast as the ticking of a pocked watch and not as slow as that of a pendulum clock, about halfway in between. A trained ear could have recognized the distinctive two-tone clicking of a lever escapement, but that detail was missed since everyone in the room had their attention somewhere else.

"Brighter... hotter... stronger... Bring the flames right here in front of you."

De Lévi took his glass rod and stood up. He walked next to a student and placed the bulbous end of the rod between the students hands. He held it there for a moment.

"Brighter... hotter... stronger..."

He moved around the table, repeating the process and gently separating the hands that were help too close together. When he was done, he went back to his seat.

"You can relax now," de Lévi told them, "but before you open your eyes, you must quench your fire. You can make it starve or drown it in water. It's all up to you."

He jotted some more notes while the students opened their eyes one by one. They exchanged puzzled looks and smiles in silence while de Lévi was scribbling.

"I'm going to recommend four of you for the study of channeling." He told them. "The rest of you will get to decide between potions making, ancient languages, and the crafting of arcane devices. Sister, what is your name?" He pointed his pen at one student who immediately started blushing.

"Jeanne." She responded.

"Jeanne," de Lévi continued, "you have a boiler inside of you that has more steam than five train engines together. I will be mentoring you personally once a month." He closed his notebook and caped his pen. "That is all for me today and I hope to see you all at the next full moon ritual."

"Brother de Lévi," a student sheepishly called. "We were told that you might give us a demonstration."

De Lévi turned around towards the older man in the corner of the room who met his gaze with a nod and a smile.

"I see." De Lévi tucked his belongings back into his satchel and grabbed his cane while he rose. "Channeling is entirely possible with the mind alone," he explained, "but we now know that it is also greatly facilitated by the use of the appropriate device."

He rotated a disc at the base of the pommel of his cane. Everyone could plainly see through the elegant cuts in the pommel that something colorful was moving inside. He closed his eyes and lifted the cane high above his head.

They immediately felt the chill.

It took no time for their breath to start fogging up. It was more than just a feeling, the whole room was now as cold as a winter night. The students exchanged looks of surprise and euphoria as they started to shiver.

With his eyes still closed, de Lévi slowly brought down his cane. When its base touched the floor, the wind started. It was a gentle breeze at first, but it grew stronger with every rising and falling of de Lévi's chest. Soon enough, pages and light objects were flying across the room and the students were crouching to avoid them.

De Lévi lifted his cane then it all stopped.

He opened his eyes and started rotating the disc at the base of the pommel.

"Brother de Lévi," a student called, "that was most eloquent. Will we learn this spell?"

"Yes," de Lévi replied with a smile. "If you study study hard, ten years from now you will master this little party trick and a whole lot more. If you study hard."

De Lévi grabbed his satchel and gave a modest bow.

"That is all for me today and I hope to see you all at the next full moon ritual. Welcome to the Blue Circle Lodge!"


# What Lurks in the Crypt

Baw pushed her rapier forward as she crawled through the narrow passage. She clenched her jaw every time she heard the scrapping sound. An hour with a whetstone and she would be able to restore the blade's edge, but the ornate hilt guard would need professional attention, the kind of attention that would core her some pretty Francs if she had any hope of wearing her blade in public again without making a fool of herself.

Baw was a field agent for Lux Co. She generally liked her job. She liked the fencing and she liked the spying. "Bordel," she muttered as she freed one shoulder from a tight squeeze. She didn't like narrow passages, she didn't like rats, and she really hated narrow passages full of rats. But now that she thought about it, it had been a while since the last encountered a rat. This might have been a good or a bad thing and something in the back of her mind told her that answering this particular questions could tell her a lot about herself and about where she was.

The needs of the moment forced her to dismiss those reflections aside as purely academic. Her carbide lamp was flickering vigorously. The bright naked white flame had been perfectly steady until now and she wasn't sure which of the three brass knobs she was supposed to adjust. Was she was simply running out of fuel? A metal grate was coming in sight and with a bit of luck, it would force her to turn around and to start making way towards a warm bath.

The grate offered no resistance. She was obviously not the first one to use this route to enter the church compound and the previous intruder had been discreet enough that priests saw to need to commission repairs to the mortar.

The narrow passage opened into a low vaulted room with three layer of alcoves covering two of the walls. Baw had reached the crypt.

She carefully replaced the grate and did  her best to dust herself away from the entry point. She was after incriminating documents. She knew that those were more likely to be found in the offices above, but she was here all alone and it seemed like a good place to start searching.

Most of the alcoves hosted stone caskets, but a few has rows of urns separated by ornate wooden boxes here and there. She found many golden crosses and jeweled chalices, but nothing resembling documents. After the fourth box, She decided to move on.

Shadows danced as she moved across the room. The lamp was a constant flicker now. One of the shadows passed right in front of her then brushed against her neck. She felt the cold sting and immediately took a step back. It was like a frostbite from a strong blizzard reaching deep below her skin.

The shadow seemed to absorb more of the light, to grow darker. It had the vague outline of a person and it was floating slowly towards Baw. She drew her rapier and slashed. The blade went right through the dark form. She slashed again.

As the blade passed through the shadows, the shaped reached for Baw's hand. The chill was so sharp that she dropped her weapon. The ringing of hardened steel echoed through the silent hallways ahead.

For several seconds, she could neither feel nor move her fingers. She took several steps back. When the sensation painfully came back, her fingers felt like old rusty cogs struggling to move.

"A ghost?" She wondered out loud. She reached for her breast pocket and pulled a straight razor. The mother-of-pearl handle was expertly crafted, the polished blade reflected light on the walls. Her next stroke was swift with her whole body extended to avoid exposing herself to the dark shape. The form twisted as the blade passed through. She could not tell how much she had hurt it, but at least this blade was doing something.

Three more slashed and she had pushed the shape back about half a meter. This would take a while. She heard faint steps coming from somewhere deep inside the hallway. *So much for a sneaky entrance*, she thought.

She kept slashing as the sound of footsteps grew closer, then suddenly the dark shape slid away from her and merged with one of the walls. She raced to retrieve her rapier the darted down the hallway ready to face whoever was coming. The flame from her carbide lamp was now dim, yellow, and sooty.

She almost tripped when her legs froze after she got close enough to see that it was not a person coming. The four legged creature was as large as a pony. It the body of a griffin with a human head perched atop a long neck. Wings were folded along its back. The creature turned its head, revealing another face, that of a large cat. It roared. The sound sent shivers down Baw's spine, it was deep and metallic, like nothing she had heard before.

Baw spun around and ran to the closest side room, placed the carbide lamp on the ground, then sheathed her rapier. The clicking of claws on the stone floor were steadily drawing closer, unhurriedly. Baw assessed her surroundings. The room was just like the first one she had been in. It was not very big, but it would have to do.

The creature entered the room. Baw could now see another face on the side of the head opposite to the cats face. It had a long dog-like snout with pointy teeth. The human face was very beautiful with fine angular features. It could have been the face of a woman or that of a man still too young to grow a mustache. The beastly torso was very muscular.

The creature kept approaching and Baw snapped out of her contemplation. Now was the time.

She sprinted towards the creature. When she was two meters away, she dove sideways and rolled with the landing. A set of claws dug into her shoulder as she moved passed the monster.

The pain came instantly, a here and now kind of pain. It was the king of paint that makes one forget about what year it is and what kind of hat they were hoping to buy with the next week's wages. In Baw's mind, there room for only two things: the source of the pain behind and the dark hallway ahead. She raced into the darkness.

She ran as fast as she could with eyes wide open even though there was nothing to see. She ran with he hand brushing the wall. She ran and tripped and fell, then got back up and ran again. She ran and fell again when she reached the stairs, then clumsily made her way up until she bumped into a wooden door with a loud thump.

Baw collapsed on the ground after passing the door. The penumbra gave just enough light to see the outline of her surroundings: a long hallways with moonlight coming from somewhere around the corner. After her breath slowed down, she pressed her ear against the wooden door. The only sound came from her still racing heart. If the creature was still chasing her, it must have been far behind.

It all came back slowly after that: she was here for documents and she liked spying. She started walking as silently as she could down the hallway. She also started recall the plans of the Church compound that she had studied before her mission. The plans had shown a wall around the perimeter of the compound. Baw had never seen the wall, but she could picture it in her mind: old and tall, aged enough to feature plenty of gaps between the stones.

Baw would climb that wall on her way out.


# Hiding in the Shadows

"I want someone behind that bush and two persons in that shed over there." The officer was giving orders while pointing with his saber. The mirror polish of the blade was scattering the orange rays of the setting sun around him.

"Where should we put the lanterns, captain?" One of the half dozen soldiers around the officer asked.

"For the fourth time, Ferland, we are here to catch grave robbers. Do you really think they will show up if we announce that we are here?" the captain replied without turning towards Ferland.

"But it's the new moon tonight, captain. We won't be able to see a damn."

"Ferland," the officer replied after a long pause, "don't you think that it would be perfect if darkness prevented the trespassers from seeing us? You can light all the lanterns you want after we have them in cuffs. I certainly would not want you to trip and injure one of your sensitive toes on the way out." The officer turned to face Ferland while the other soldiers chuckled.

Captain Filo was also annoyed by this assignment, but he knew better than to let it show. There was simply no pattern to the grave robbing that had started a few months prior. There were a few graves of rich people that got dug open, but they dug mostly poor people out. If they were after jewels, their strategy was flawed. Some body parts were sometimes missing, but then again, there was no pattern. They seemed to come any day of the month, but they also seemed to prefer dark and cloudy nights. This was the only fact that gave the captain some confidence that they would see some action tonight rather than being posted here every night for the next three weeks.

"That will be all, soldiers. Go get a warm meal and come back in small groups to take your positions before 21-o'clock. If anyone comes back drunk I will have them whipped. Dismissed!"

***

Crouched in a corner of the tool shed, Ferland was jittery. In retrospect, downing half of that second pot of coffee had been a bad idea, but after the captain's speech, he had no intention of falling asleep on duty. His bladder seemed to agree with the assessment.

"I need to take a leak, " he told Alice who was sitting on a crate at the opposite corner. Alice quietly nodded. Ferland hated how she could remain perfectly calm and fully alert all at the same time.

Ferland struggled to exit the shed quietly. There were various landscaping implement hanging from all the walls and his long rapier was threatening to knock them down whenever he moved. The night was as dark as coal,so he didn't have to go far to gain some privacy. It was mostly a matter of feeling around to make sure he would not relieve himself on a tomb. Ferland was not religious nor particularly superstitious, but he had seen his share of comrades expire in combat and that gave him some respect for the dead.

Beyond the cemetery walls, the city was still alive. Carriages were moving about and the laughter of revelers leaving absinthe lounges filled the air. Much closer, crickets and an occasional owl were playing tricks with Ferland's nerves.

He knew he was not alone – there were five other soldiers hidden in nearby bushes – but he also didn't want to let an intruder sneak by while he had his pants down. The smells were off. It smelled mostly of grass and freshly turned dirt, but there was something else, not rotting bodies, which Ferland was familiar with. It was something peculiar, musty, but he could not put his finger on it.

Ferland hurried with his business. The night was till, but shadows were dancing around him. Ferland had never studied optics, but he knew that this was wrong. In his mind, a still light source shining on a still object should be casting still shadows. He listed more carefully, trying to pick the rustle of leaves. Nothing. There was no wind tonight.

He started heading back towards the shed. Then he saw the movement in the corner of his eye, he was sure of it. A shadow as tall as he was had just slid behind a bush. The grave digger was more silent than he expected, but Ferland had spotted him now and he would be the one catching him.

Ferland crouched, lifting the scabbard of his rapier to prevent it from noisily scrape the ground. He peered into the darkness, listening carefully. Right there, behind the bush, was definitely something moving.

Ferland reviewed his options while his heart rate kept increasing. He could call for reinforcement and corner the intruders, he could face them sword drawn and a ask for surrender, but since the intruder was most certainly alone, Ferland could also jump over the bush and tackle him into submission. Ferland knew how to wrestle guys much bigger than himself and he felt good about this plan. The blood rushing in his veins was making a deafening thumping in his ears. He had to move soon.

Ferland sprinted towards the bush and leapt with arms extended ready to grab onto a torso or a leg. He landed face first with a thud.

Ferland rolled over and quickly reassess the situation. He had obviously revealed his position now and he had to react fact. He scanned his surroundings but nothing seemed to make sense.

The dark figure was still there, standing slightly taller than the bush. Had he dodged? Ferland sprang to his feet and drew his rapier.

"Don't move!" He called.

Everything was still except Ferland's sword point which was oscillating a lot more than he liked. Then the figure started heading closer.

"Don't move!" He called again. "I'm not alone here. You are surrounded."

The dark shape kept drawing closer. Ferland spurt into a warning strike, aiming for the left shoulder. He missed.

Or did he? His blade was extended right through the shader where a shoulder should have been, but he had felt to resistance at all on the way in. He pushed back two steps. He squinted trying to make sense of it all. The dark shape kept advancing. Ferland jumped forward aiming for the abdomen.

He felt no resistance as his blade went through. Whatever it was, the dark shape was not solid. The shape touched Ferland's extended right knee.

"Putain!" he let out as he fell to the ground. The touch had felt like a cold bayonette penetrating his flesh and could no longer flex his knee.

He rolled away as best as he could. The sound of racing footsteps were drawing closer. Ferland moved his blade facing their general direction.

Captain Filo appeared saber in hand. "Fall back you dim!" he called while switching to the en-garde stance. Ferland kept rolling until he was four steps behind the captain.

He could see the captain slashing with a steady cadence. The saber was passing right through the dark shape with every strike. Convulsions were moving through the shape when the blade swept. The captain kept pushing his way forward. Two soldiers with lanterns in hand join the scene.

In the dim light of the lanterns, all doubt escaped Ferland's mind. "Bordel..." muttered the man to his left. Several steps away, the captain was slashing at a transparent mass just slightly darker than the surrounding night. The dark mass was clearly retreating now.

"There are ghosts here, captain" Ferland called out when Captain Filo started heading back towards them.

"Of course there are ghosts here", the captain sneered with exasperation. "That's why we build those places, to the ghosts stay in rather than wandering the streets."

"I thought..." Ferland trailed.

"You thought what?" The captain asked. "You thought that you knew better that to follow orders? You were assigned a weapon for this mission, a saber just like this one and I was there when you picked it up from the armory. Did you think of getting a few Francs by pawning it?"

"No, no captain, " Ferland stammered. "I thought that since I have a lot more training with the rapier I would be better armed with a familiar blade. I was not expecting... those." He gestured in the direction where the captain had been a moment earlier.

All the soldiers were now assembled, most of them carrying a kerosene lantern. "Anyone else had some great idea to make the night more interesting?" the captain asked them.

"I also came with my rapier, captain," Alice replied sheepishly, "for the same reasons."

"Help this man to his feet," Captain Filo snapped, "then out of my sight! All of you! We won't catch anyone tonight with all this racket."

They left in silence, slowly letting the night envelope Captain Filo.

"There is a lounge three blocks from here, Ferland," the man who was helping him walk said after a while. "You look like you could use a date with a magical lady dressed in green."

"No, thank you," replied Ferland after carefully considering the offer. "Maybe tomorrow, but I've had enough surreal for today."


# The Sound of Satin
[moved here](../../src/story/sound_of_satin.md)


# The Sands of Time

*October 26, year 90 AP*

Gilles was afraid. He had woken up screaming last night and his breaches were still damp with his own urine. It didn't matter anymore. Any moment now and they would take him out from this cell and bring him to the gallows. The sound of the cheering crowd told him that much. He felt the texture of the stone wall with his hand and filled his lungs with the aroma of straw and piss, savouring his ability to feel things while it lasted.

Gilles had always been afraid of death. It was hard to avoid growing up with his grandmother telling him tales of watching people fall like flies during the plague. It had been nine years since that fear became an obsession. That was when the Saxons took Joan and burnt her alive. Even her, with all her magic, had been unable to escape the scythe of the reaper.

The crowd was growing louder. Gilles was guilty and he had confessed everything. Almost everything. Now that it was almost over, he was glad that they had never asked why, what he had hopped to gain by taking the lives of all those children. In a moment, he would give the crowd what they were asking, he would die for their collective enjoyment. If the books had been right, he would die, but he would not cease living – maybe he would succeed where Joan had failed. Maybe...

The pages had survived the plague years, but no one who knew how to interpret them had and for almost a century they had been waiting, gathering dust, waiting for someone who was desperate enough to find them and let light shine on their arcane words again.

Gilles heard footsteps coming his way. He closed his eyes and wept softly, unsure if any of it had been worth it.


# Bewitching Bookkeeping
[moved here](../../src/story/bewitching_bookkeeping.md)


# Drums of Disruption

Yrganiv was sitting at his mirror redoing the braids in his long facial hair. As a Pacherr, he had a lot of those. Franconians sometimes referred to Pacherrs as "elephant people", which was vaguely offensive given that unlike Yrganiv, elephants walked on all four, were bald, and couldn't play musical instruments. Whatever they wanted to believe, that was fine with him as long as they came to his concerts.

He was already dressed and he was putting the final touch to his stage look. In his pocket, the small wooden box was wound up and ticking. His Zora, he called it. It was made of oak and brass, full of tiny gears inside. His big fingers would never be able to work on suck an elaborate mechanism; he had commissioned Zora in Vienna, after working two years to find and acquire the two crystals at its core: a tourmaline and an emerald.

When Yrganiv concentrated on the box, he could make his instruments play louder, much louder. His drums and his trombone would be thundering through the neighbourhood, as would all the instruments of his fellow musicians and the steps of the dancers as long and they didn't wander too far. That's how they were able to draw large crowds in all the cities they visited.

Sitting at the other corner of the wagon, Mara and Jacek were putting the final touch to a new song. They had a few bottles of wine to feed their creativity and Yrganiv was doubtful that those had been legally acquired. Wine was just too expensive anywhere in Franconia. Whatever they wanted to do in their freed time. Yrganiv didn't mind as long as they played well on concert nights.

Yrganiv thought he heard something brushing against the wagon. He closed his eyes and sent this thoughts through Zora, being careful to make the sounds louder only to himself. The whispers outside came out as clear as if someone was shouting next to him.

"So much for tonight's concert," he muttered as he got up. In two steps in was at the door. He grabbed one of the sabers from the basket there, the one with the largest guard. This one was heavy and sharp.  It was not a stage prop.


# Rising

The vampire awakened. It was not like someone emerging from a dream, but more like a newborn painting its first thought onto a blank canvas. The thoughts were simple at first and then gained more structure as time progressed.

Time. There had been a previous time. Time was measured in days and then in seasons. How long had he stopped thinking? There was no way for him to know, but he was still alive.

Life. He needed more of it. With more life, he would be able to think clearly again, be whole. He had no physical sensations, only thoughts. Or did he? That constant background thought without a start or an end might have been feeling cold.

Retz. That was a name. His name. People had called him this way in a previous time. The name brought him pleasure, but it didn't feel right. Maybe it was better to leave it in the previous time.

Steps. The vampire could not see or hear, not yet, but he could sens the vibrations of someone walking nearby. The glimpse of a craving appeared and then grew to eclipse most of his consciousness. He could not act on the craving, not yet, but now his existence had a direction.

Patience. He understood time once more and enjoyed contemplating its flow. He could now read the message in the steps. Someone had come to offer him more life. Whether they were doing it willing or not was delightfully irrelevant.


# Contact

The clouds over Lyon were heavy and gusts of wind were lifting dust in the narrow streets. Philippe quickly turned a corer and dashed though an unmarked door. He was eager to leave the street before the rain started. Few people knew that this door was unlocked because unlike most traboules, this one was not a shortcut to anywhere in particular. It had a few other exits, as all traboules had, but this one would have been a detour for most traveller, perfectly pointless unless you really needed a roof over your head for the next few blocs.

What this traboule did provide was a discreet way to travel and an access to several storage rooms – both related to Philippe's current journey. The upper level of the corridors was lined with doors and courtyards. People lived and operated looms here and they all knew Philippe. Deeper down, you encountered fewer rooms and they stone work of the walls was a lot more utilitarian, even a little rushed sometimes. It was a good idea to carry a sword when wandering this far from the eyes of the law, which Philippe did.

Philippe knocked at a door then entered without waiting for an answer. He greeted the family living there and then went straight to the loom to admire what the canut – the master weaver – was working on. It was an elaborate pictorial pattern over a blue background with six colours and some gold threads. At a glaces, Philippe estimated a little over ten thousand perforated cards in the loom's pattern stack.

"You'll be working on this one for a while, Alain," Philippe said.

"I was sick of flowers," Alain said without losing a beat moving his many shuttles left and right and without lifting his eyes from his work. "I won's see a Franc until this roll is done in three weeks, but it pays better in the end."

"Save us the trimmings from that gold thread," Philippe said. "We can melt it, it's untraceable funding for our operations."

"There are no trimmings, Philippe. Sophie loads my shuttles and they never snag." A girl about twelve years old looked up from her desk with a beaming smile. "What brings you here anyway?" Alain asked.

"I need to borrow your loom." Philippe said.

Alain chuckled. "You come back next month. I'm not rethreading anything until this is done."

"No," Philippe said, "the other loom."

Alain stopped working and turned around. "There was a message?" he asked.

"I think so," Philippe said handing him a bundle of eighty or so perforated cards.

Alain studied the cards in licence, lifting them in front of the tall window to see the holes better. "Yeah," he said after a while. "It starts on cards number seventeen. Sophie, give him the blue key." He went back to his work after giving Philippe the cards back.

Sophie went to a shelf piled high with cards. She retrieved a bundle of a dozen or so yellow cards and gave them to Philippe. What made those more blue than any of the other cards on the shelf was completely lost on him. He walked to the backroom where another loom was taking most of the space. There was a flower pattern showing on the half-done canvas.

Philippe noted which cards was engaged and then proceeded to change the cards stacks: the message in the main overhead loader, and the key in the smaller side loader that was previously unused. He didn't touch any of the threads. He was not going to advance the canvas, only activate the cards and note which pins would lift.

Alain called from the other room: "Burn the key when you are done. It's a one-time."

"Sure thing," Philippe called back. "Want me to burn the flower cards while I'm at it?"


# Barrels and Bravery

Pridout was waiting on the street corner. Thanks to his contact being late, he had been there for a while now.

He whistled at a woman across the street and winked a few times. He didn't find here attractive, but it seemed like a good time to advertise a plausible reason for him being there. Somewhere nearby were bundles of untaxed silk and mechanical contraptions he want's too sure what the purpose of was. As a smuggler, finding a purpose for cargo was not his problem. Moving it discreetly was.

He considered going to grab a sandwich. They always made good sandwiches in Lyon. He did the whistling routine again, with a Pacherr this time. He was not certain about the gender, but it was unlikely to get him in trouble. Getting his ship inspected was what he was worried about.

He would have to pay import duty on the Sicilian wine, that was bad enough, and where was a whole array of chemicals that he had never heard of ten years prior. Some even came with instructions on what could and could not be stacked together. Those probably spelled real trouble. He had destroyed the stacking instructions after the ship had been loaded. This provided him with plausible deniability, but he now regretted doing so a little. Whoever was going to buy those "nitrates" had better be up to speed on the ins and outs of storing them.

A door opened. "Monsieur Pridout, " someone called from inside, "you should have come in."

"Come in where?" Pridout asked at the unseen voice.

"The traboule, Mr. Pridout. This one is never locked on Tuesdays. Let me show you the crates we wish to ship."


# Birth Dice

"Don't let him play with his brass dice," the inn keeper said as he placed two mugs of beer on the table "they're loaded."

"They're not loaded, they are just my lucky birth dice," objected the young man who was sitting down. He took three metal dice from a leather pouch and set them carefully on the table. "And they are not made of brass, they're cast bronze. An absinthe please, René."

"The heck are birth dice?" Asked the woman sitting to his right.

"Those were rolled to decide who my parents would be, maybe also other things that they never told me about."

"How the heck can you decide who your parents are? The woman replied. "That's not how babies are made, boy!" Laughters burst around the table.

The young man carefully arranged his glass and spoon under the water fountain that had just arrived. "Of course I was not the one who did the roll, a government official did it for the marriage lottery. Five francs stake on the next game?"

Skeptical looks were exchanged around the table, then a nod, and then a silent consensus was reached.

"Sure," said a bearded sailor with a twitchy face, "but we all get to use your dice. You Tripolitan? They do random marriages over there."

"That's where my parents met, but I was born in Marseille," said the young man. He tossed a few coins in the center of the table and carefully handed two of his dice to the sailor.

"I guess you can't pick an accent like that anywhere else," the sailor said, attracting another burst of laughter. He added some coins to the pile and rolled the dice. Nothing good came up, so we passed the dice to the old man to his left.

"I've been to Marseille," said the new player who added his coins to the pile. "I ate an exquisite tajine there. What brings you to Lyon?" He rolled, but didn't win anything.

"I make bicycles," the young man replied. "I rolled my birth dice to select a city and they told me I should open a shop in Lyon."

The woman added her coins to the pile. "You seen the hills around here? It's the doctors who are going to win if you sell any bikes," said the woman. After the new waves of laughter died down, she rolled. Nothing.

The young man accepted the dice back and took a sip of his absinthe, which brought a smile to his lips. "It's true that canuts move about faster down the traboules than they would out on the streets, event if they had a good set of wheels," he said. "But not everyone is a canut and sometimes you are better of listening to your gut rather than over analyzing flawed assumptions." He rolled and won the pot. "Some call it believing in your luck."

Dumbfounded looks were exchanged around the table while the young man pocketed his winnings.

"You switched one of the dice, that's how you did it!" Said the old man.

"Don't you think I would be playing cards if I was into those tricks?" The young man stood, bowed, and left.


# The Forge

The ringing of metal was loud in the workshop. A woman in her twenties was feeding a boiler while the blacksmith was working a long metal rod under the steam powered hammer. Every few seconds, a jet of steam would hiss and the large mass would drop loudly on the glowing rod, sending sparks in all directions. Both were clad with a leather apron and wore goggles.

The blacksmith knew by feel when to move his work piece. "I will draw this to the right length," he said putting the rod in a bed of hot coals, "then we can shape the blade cross section. I think a narrow fuller near the point would look good on this one."

"Whatever you say, Boss." The apprentice said. "It's forging tools that I'm interested in. I don't think we'll be making blades much longer once they make duels illegal."

"I lost one of those when I was your age," the blacksmith said. "It got me this scar." He pointed as his muscular forearm. In the hot and dusty environment the many scars were obvious, unlike the skin around them, they all seemed to stay clean. Which particular scar he was referring to was not immediately obvious. "Now when someone insults me, I let it slide and I know the bloke is the kind of folk to whom I can sell a rapier."

"Why are they doing it? Why risk your life just to prove a point?" The apprentice asked.

"You'll see when this one is done. A good blade is a beautiful thing," the blacksmith said. "If we get the balance right it feels part of yourself, it improves the parts that nature made a little weak."

The rod was now white hot and it went back under the hammer for a few minutes.

When rod came back in the hot coals, the apprentice asked: "So they think they are better than everyone because they have a good sword?"

The blacksmith shrugged. "You don't forge a wrench like you forge a rapier. Some folks are stiff, some have more flex and you can't change them much after they've cooled down. If you want to live old, you'd better learn how to tell one from the other by how they ring." He took the blade from the coals and looked at it from various angles. "Here, make this sections rectangular, than taper it to a flat point around here."

"You want me to the do this part?" The apprentice asked.

The blacksmith gestured towards the steam hammer with his chin then went to grab a cup of water. He watched his apprentice work in silence.

"Where's the balance point now?" He finally asked.

"About here", she said, pointing at the blade.

"This is not an axe. Draw the point thinner and cut the excess. You want the balance close to you if you want to say in control." The blacksmith said. "It's the same if you want to say in business, you have to control the changes in direction."

"I don't get it," the apprentice said.

"Make the base of the blade heavier than the point," the blacksmith said.

"I get that part," the apprentice said. "What's the deal with staying in business?"

"Gears." The blacksmith said. "The more gears there are in a machine, the less margin you have for imperfections.  Machines are not getting simpler, so get good at making gears. Also, go to the bars near the port. Beer is terrible there, but that's where you can guess where your opponent's blade is about to move."
