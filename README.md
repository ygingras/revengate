Revengate
=========

This is Revengate, a roguelike dungeon crawler for Android set in a Steampunk universe.

More about Revengate:
* [Website](http://revengate.org)
* [Git repo](https://gitlab.com/ygingras/revengate/)
* Google Play ([here](https://play.google.com/store/apps/details?id=org.revengate.revengate) or [here](https://play.google.com/apps/testing/org.revengate.revengate))

## Copying

Revengate is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.


## Contributing

Please note that this project is released with a Contributor Code of Conduct. By
participating in this project you agree to abide by its terms.  See
CODE_OF_CONDUCT.md for the details.

## Credits
Yannick Gingras and contributors
See CREDITS.md for the details.
