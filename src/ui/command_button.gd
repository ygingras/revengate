# Copyright © 2023–2024 Yannick Gingras <ygingras@ygingras.net> and contributors

# This file is part of Revengate.

# Revengate is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Revengate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Revengate.  If not, see <https://www.gnu.org/licenses/>.

@icon("res://src/ui/cmd_button.svg")
## A Button specifically made to run a CommandPack.Command
class_name CommandButton extends Button

const REPEAT_DELAY = 0.4  ## wait that long until auto-repeating a command can happen for the first time
const MIN_DELAY = 0.1  ## subsequent repeats come faster, until the delay is only this fast

var cmd:CommandPack.Command
var coord:Vector2i
var hero_pov:bool
var still_down := false
@onready var cooldown_timer := Timer.new()
var is_cool := false  ## the repeatable command has cooled down

func _init(command, coord_, hero_pov_:bool, char_only=false):
	focus_mode = Control.FOCUS_NONE
	action_mode = BaseButton.ActionMode.ACTION_MODE_BUTTON_PRESS
	cmd = command
	coord = coord_
	hero_pov = hero_pov_
	if char_only:
		text = cmd.char
		theme_type_variation = "ProminentButton"
	else:
		text = cmd.caption
		if cmd.is_action:
			theme_type_variation = "ActionBtn"
	button_up.connect(on_button_up)
	button_down.connect(on_button_down)

func _ready() -> void:
	add_child(cooldown_timer)
	cooldown_timer.wait_time = REPEAT_DELAY
	cooldown_timer.one_shot = true
	cooldown_timer.timeout.connect(_on_cooldown_timer_timeout)
	reset_retrigger()

func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and not event.pressed:
			accept_event()
			reset_retrigger()

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if not event.pressed:
			reset_retrigger()

func _shortcut_input(event):
	if disabled:
		return
	if cmd.ui_action and event.is_action_pressed(cmd.ui_action):
		accept_event()
		pressed.emit()
		activate()

func activate():
	## Run the command and do some state management to prepare for possible auto-repetition
	still_down = true
	var acted: bool
	if hero_pov:
		acted = await cmd.run_at_hero(coord)
	else:
		acted = await cmd.run(coord)

	if cmd.is_repeatable:
		is_cool = false
		var delay = max(MIN_DELAY, cooldown_timer.wait_time * 0.8)
		cooldown_timer.start(delay)
			
	if acted:
		Tender.hero.finalize_turn(acted)

func try_retrigger():
	## Try to re-run the command
	if disabled or not cmd.is_repeatable or not still_down or not is_cool:
		return
	activate()

func reset_retrigger():
	## Reset all the retriggering states
	if not cmd.is_repeatable:
		return
	still_down = false
	cooldown_timer.stop()
	cooldown_timer.wait_time = REPEAT_DELAY
	is_cool = false
				
func reset_visibility(coord_:Vector2i, index:RevBoard.BoardIndex):
	coord = coord_
	cmd.index = index
	if hero_pov:
		visible = cmd.is_valid_for_hero_at(coord)
	else:
		visible = cmd.is_valid_for(coord)
		text = cmd.caption

func on_button_up():
	reset_retrigger()

func on_button_down():	
	activate()

func _on_cooldown_timer_timeout():
	is_cool = true
	try_retrigger()
	
