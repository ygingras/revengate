Ivry was sipping a terrible beer right outside a café near the city walls.

*clack clack... clack clack*

The heartbeat of looms was echoing through the narrow streets. Despite the horrible drink, Ivry was not able to repress a smile. That sound was music to his ears, it was music to pretty much everyone's ears in Lyon, a song about the recovering economy with the weavers all busy working.

*clack clack... clack clack*

Of the thousand looms between here and the ships waiting for their cargo of fine silk, Ivry could tell that one sounded too hollow. Ivry, a carpenter and loom mechanic, smiled some more – the owner of that hollow loom would hire him later this week.

It was dinner time for regular folks like Ivry, but the canuts – how master weavers were called around Lyon – were still working and they would keep at it as long as the light was good.

They'd be pouring in the cafés after sundown. Oh yeah, they would be all here and unlike Ivry, many of them would be ordering wine. Lucky bastards! Ivry was not mad, he was just nostalgic of the old times when he would be sipping a light Chardonnay on a hot day like today.

*clack clack... clack clack*

A bloke caught Ivry's attention. Nom d'un rat! Ivry thought. Who bought books in a café? Pornography? Ivry had seen those stories and he couldn't see much of the point. Besides, the buyer was dressed well enough that he should have been able to afford a visit to one of Lyon's many brothels. Dressed like that, he could have been a trader, but a trader would never show up in this part of town with a cravate made of... wool? Ivry squinted. Merino wool! It was very well made, but it was still a faux pas.

*clack clack... clack clack*

After years of living here, Ivry could close his eyes and see the cogs moving. He could see where the machine was straining and where a small adjustment and a little lubrication would lead to greater efficiency, efficiency in Ivry's hydration routine.

Ivry slowly pushed his glass to allow the beer inside to warm up in the full sun, then he went for a cigarette closer to where he could hear what the bloke was bragging about.
